import { Location } from "./Location";

export class Asset {
    public constructor(
        private _identifier: string,
        private _name: string,
        private _location: Location,
        private _description?: string
    ) {}

    public get identifier(): string {
        return this._identifier;
    }

    public set identifier(value: string) {
        this._identifier = value;
    }

    public get name(): string {
        return this._name;
    }

    public set name(value: string) {
        this._name = value;
    }

    public get location(): Location {
        return this._location;
    }

    public set location(value: Location) {
        this._location = value;
    }

    public get description(): string|null {
        return this._description;
    }

    public set description(value: string) {
        this._description = value;
    }
}
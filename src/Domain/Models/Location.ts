export class Location {
    public constructor(
        private _xaxis: number,
        private _yaxis: number
    ) {}

    public get xAxis(): number {
        return this._xaxis;
    }

    public set xAxis(value: number) {
        this._xaxis = value;
    }

    public get yAxis(): number {
        return this._yaxis;
    }

    public set yAxis(value: number) {
        this._yaxis = value;
    }
}
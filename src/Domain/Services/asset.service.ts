import {Asset} from "../Models/Asset";
import {Location} from "../Models/Location";

export class AssetService {
    public async create(): Promise<Asset> {
        const location = new Location(12.248912, 1.339134);
        const assetDto = new Asset("1234-5678-9012-3456", "Test asset for the demo", location);

        return assetDto;
    }

    public async findAll(): Promise<Array<Asset>> {
        return [];
    }

    public async findOneById(identifier: string): Promise<Asset> {
        const location = new Location(12.248912, 1.339134);
        const assetDto = new Asset("1234-5678-9012-3456", "Test asset for the demo", location);

        return assetDto;
    }
}

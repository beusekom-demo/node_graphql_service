import {Field, ObjectType} from "@nestjs/graphql";

@ObjectType()
export class Location {

    @Field()
    public xaxis: number;

    @Field()
    public yaxis: number;
}
import { Field, ID, ObjectType } from '@nestjs/graphql';
import {Location} from "./Location";

@ObjectType()
export class Asset {
    @Field(type => ID)
    public identifier: string;

    @Field()
    public name: string;

    @Field({nullable: true})
    public description?: string;

    @Field(type => Location)
    public location: Location;
}
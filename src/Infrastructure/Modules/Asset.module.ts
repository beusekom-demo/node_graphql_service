import { Module } from '@nestjs/common';
import {AssetResolver} from "../Resolvers/AssetResolver";
import {AssetService} from "../../Domain/Services/asset.service";

@Module({
    providers: [AssetResolver, AssetService]
})
export class AssetModule {}
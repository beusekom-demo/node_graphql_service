import {AssetService} from "../../Domain/Services/asset.service";
import { Args, Query, Resolver, Subscription } from '@nestjs/graphql';
import { PubSub } from 'apollo-server-express';
import {Asset} from "../Models/Graphql/Asset";
import {Location} from "../Models/Graphql/Location";
import {NotFoundException} from "@nestjs/common";

const pubSub = new PubSub();

@Resolver(of => Asset)
export class AssetResolver {
    public constructor(private readonly assetService: AssetService) {}

    @Query(returns => Asset)
    public async Asset(@Args('identifier') identifier: string): Promise<Asset> {
        const asset = await this.assetService.findOneById(identifier);
        const locationEntity = new Location();
        locationEntity.xaxis = asset.location.xAxis;
        locationEntity.yaxis = asset.location.yAxis;
        const assetEntity = new Asset();
        assetEntity.identifier = asset.identifier;
        assetEntity.name = asset.name;
        assetEntity.description = asset.description;
        assetEntity.location = locationEntity;

        if (!assetEntity) {
            throw new NotFoundException(identifier);
        }

        return assetEntity;
    }

    @Subscription(returns => Asset)
    assetAdded() {
        return pubSub.asyncIterator('assetAdded');
    }
}
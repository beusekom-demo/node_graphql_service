## Description
This service has been created to show a working implementation of a Graphql service using Node as the environment. This 
application also intends to show a working docker environment for local development, including HMR. Lastly, the current
environment shows a CI implementation using Gitlab. This demo was created using the 
[Nest](https://github.com/nestjs/nest) framework, due to it already having a default implementation for Graphql, 
dependency injection, and Jest.

## Start service
```bash
$ ./bin/start
```

## Using service
The service is reachable on [localhost:3000/graphql](http://localhost:3000/graphql)

## Running unit tests

```bash
$ ./bin/npm run test
```

## Running end to end tests

```bash
$ ./bin/npm run test:e2e
```
